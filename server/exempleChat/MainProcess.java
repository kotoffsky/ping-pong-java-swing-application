package server.exempleChat;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainProcess {
	//private List<GestionnaireClient> clients;
	private ServerSocket serveurSocket;
	private Map<String, GestionnaireClient> mapPseudo;
	
	public void lancerServeur() {
		try {
			serveurSocket = new ServerSocket(5555);
			//clients = new ArrayList<>();
			mapPseudo = new HashMap<>();
			while (true) {
				{
					System.out.println("Attente de clients : accept()");
					Socket socket = serveurSocket.accept();
					System.out.println("accept() est debloqu� : client connect�");
					new GestionnaireClient(socket,this);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void broadcast(String pseudo, String msg){
		for(GestionnaireClient gc : mapPseudo.values()){
			gc.envoyerMsg(Protocole.LECTURE_COLLECTIF, pseudo, msg);
		}
	}
	
	public void emit(String pseudo, String msg, String receiver) {
		((GestionnaireClient)mapPseudo.get(receiver)).envoyerMsg(Protocole.LECTURE_PRIVEE, pseudo, msg);
	}
	
	public void addGestionnaire(String pseudo, GestionnaireClient gc) {
		mapPseudo.put(pseudo, gc);
	}
	
	public static void main(String[] args) {
		new MainProcess().lancerServeur();
	}

	public Map<String,GestionnaireClient> getClients() {
		return mapPseudo;
	}

	public void setClients(Map<String,GestionnaireClient> clients) {
		this.mapPseudo = clients;
	}
	
	

}
