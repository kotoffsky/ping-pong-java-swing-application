package server.exempleChat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Collection;

public class GestionnaireClient implements Runnable {
	private PrintWriter out;
	private BufferedReader in;
	private String pseudo;
	private MainProcess main;

	public GestionnaireClient(Socket s, MainProcess main) {
		this.main = main;
		try {
			out = new PrintWriter(new OutputStreamWriter(s.getOutputStream()));
			in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			new Thread(this).start();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void run() {
		String inputMsg;
		while (true) {
			try {
				inputMsg = in.readLine();
				switch (inputMsg) {
				case Protocole.PSEUDO:
					synchronized (main) {
						this.pseudo = in.readLine();
						main.addGestionnaire(pseudo, this);
						Collection<GestionnaireClient> list = main.getClients().values();
						out.println(Protocole.ENV_LIST);
						out.println(list.size());
						for (GestionnaireClient c : list) {
							out.println(c.pseudo);
						}
						out.flush();

					}
					break;

				case Protocole.ECRIRE_COLLECTIF:
					inputMsg = in.readLine();
					main.broadcast(pseudo, inputMsg);
					break;
				case Protocole.ECRIRE_PRIVEE:
					String receiver = in.readLine();
					inputMsg = in.readLine();
					main.emit(pseudo, inputMsg, receiver);
					break;
				default:
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	public void envoyerMsg(String typeDeLecture, String pseudo, String msg) {
		synchronized (main) {
			out.println(typeDeLecture);
			out.println(pseudo);
			out.println(msg);
			out.flush();
		}
	}

}
