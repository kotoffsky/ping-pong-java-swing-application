package server.exempleChat;

public interface Protocole {
	 String PSEUDO = "PSEUDO";
	 String ENV_LIST = "ENV_LIST";
	 String ECRIRE_COLLECTIF = "ECRIRE_COLLECTIF";
	 String LECTURE_COLLECTIF = "LECTURE_COLLECTIF";
	 String ECRIRE_PRIVEE = "ECRIRE_PRIVEE";
	 String LECTURE_PRIVEE = "LECTURE_PRIVEE";
}
