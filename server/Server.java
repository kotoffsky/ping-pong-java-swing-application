package server;

import java.io.DataInputStream;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import model_client.Jeu;

public class Server {
	
	/**
	 * Socket du serveur 
	 */
	static ServerSocket serverSocket;
	/**
	 * Socket pour gerer les connexion au serveur
	 */
	static Socket socket;
	/**
	 * Map: Liste de login des utilisateurs
	 */
	static Map<String, PlayerManager> playerMap;
	
	static Jeu jeu;
	/**
	 * Parametrage pour le démarrage du serveur
	 */
	public void startServer(){
		try {
		
			System.out.println("Démarrage du serveur...");
			serverSocket = new ServerSocket(5555);
			System.out.println("Serveur demarré...");
			playerMap = new HashMap<String, PlayerManager>();
			System.out.println("En attent de connexions...");
			jeu = new Jeu();
			while(true){
				//On attend la connexion d'un utilisateur
				socket = serverSocket.accept();
				System.out.println("Connection from : "+ socket.getInetAddress());
				//On crée une instance du gestionnaire d'utilisateur pour gérer le thread
				new PlayerManager(socket, this);
				//si la map de logins existe on lance le gestionnaire de la balle
				if(Server.playerMap.size() == 0){
					System.out.println("Balle crée");
					new GestionnaireBalle(this);
				}
			}
		} catch(IOException e) {
			//e.printStackTrace();
			System.out.println("Adresse du seveur déjà utilisé!");
		}
		try {
			serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
			serverSocket = null;
		}
	}
	
	/**
	 * Démarrage du serveur
	 * @param args
	 */
	public static void main(String[] args) {
		new Server().startServer();
	}
	
	/**
	 * Envoie d'un objet (user, balle) à tous les joueurs
	 * @param obj Objet à envoyer
	 * @param action String de l'action du protocole
	 */
	public void broadcast(Object obj, String action){
		for (PlayerManager pm : playerMap.values()){
			pm.emit(obj,action);
		}
		
	}
	/**
	 * Ajout d'un nouveau utilisateur au Map 
	 * @param pseudo
	 * @param pm
	 */
	public void addPlayerManager(String pseudo, PlayerManager pm){
		playerMap.put(pseudo, pm);
		System.out.println(pseudo + " ajouté");
	}
	/**
	 * Accesseur du Map des logins des utilisatuer
	 * @return le map
	 */
	public static Map<String, PlayerManager> getPlayerMap() {
		return playerMap;
	}
	
	/**
	 * Mutateur du Map des logins des utilisateurs
	 * @param playerMapue Nouvelle Map 
	 */
	public static void setPlayerMap(Map<String, PlayerManager> playerMap) {
		Server.playerMap = playerMap;
	}
	
}
