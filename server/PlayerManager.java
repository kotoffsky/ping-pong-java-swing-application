package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Set;

import model_client.User;

public class PlayerManager implements Runnable{
	/**
	 * Flux de sortie on passe un objet
	 */
	public ObjectOutputStream out;
	/**
	 * Flux d'entre on reçoit un objet 
	 */
	public ObjectInputStream in;
	
	/**
	 * Instance du serveur
	 */
	private Server server;
	/**
	 * 
	 */
	private Socket socket;
	/**
	 * Utilisateur connecté 
	 */
	private User user = null;
	
	/**
	 * Création du Thread pour chaque utilisateur connecté
	 * 
	 * @param socket
	 * @param server
	 */
	public PlayerManager(Socket socket, Server server) {
		this.server = server;
		try {
			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
		Thread thread = new Thread(this);
		thread.start();
	}
	
	/**
	 * 
	 */
	@Override
	public void run() {
		String action;
		while(true){
			try {
				action = (String) in.readObject();
				System.out.println(action);
				switch (action) {
					case Protocole.PSEUDO :
						System.out.println("Message du serveur : "+ Protocole.PSEUDO);
						synchronized (server) {
							user = (User) in.readObject();
							//System.out.println(user);
							server.addPlayerManager(user.getNom(), this);
							Set<String> logIns = Server.playerMap.keySet();
							out.writeObject(Protocole.USER_LISTE);
							//System.out.println(logIns);
							out.writeObject((Integer)logIns.size());
							for(String login : logIns)
								out.writeObject(login);
							out.flush();
							server.broadcast(user,Protocole.USER_CONNECTE);
							break;
						}

					case Protocole.RAQUETTE_DEPLACEMENT :
						System.out.println("Message du serveur : "+ Protocole.RAQUETTE_DEPLACEMENT);
						user = (User) in.readObject();
						
						server.broadcast(user,Protocole.RAQUETTE_DEPLACEE);
						break;
					
					case Protocole.CLOSE:
						break;
				}
				
				
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		
	}
	/**
	 * Envoie d'un objet sauf à l'utilisateur courant
	 * @param obj Objet à envoyer
	 * @param action Action du protocole
	 */
	public void emit(Object obj,String action){
		try {
			if((Object) obj != this.user){
				out.writeObject(action);
				out.writeObject(obj);
				out.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
