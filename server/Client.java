package server;

import java.io.IOException;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import model_client.Balle;
import model_client.Jeu;
import model_client.User;
import vue_client.Balle2D;
import vue_client.VueJeu;

public class Client implements Runnable{

	private Socket socket;
	private ObjectInputStream in;
	private ObjectOutputStream out;
	private String action;
	private User user;
	private List<User> userList;
	private Balle balle;
	private int ballesJouees = 1;
	
	public Client(User user) {
		this.user = user;
		init();
	}
	
	public void init(){
		try {
			System.out.println("Connecting...");
			socket = new Socket(InetAddress.getLocalHost(), 5555);
			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
			System.out.println("Connection successful");
			out.writeObject(Protocole.PSEUDO);
			out.writeObject(user);
			out.flush();
			System.out.println("Utilisateur crée avec le nom :"+user.getNom());
			
			userList = new ArrayList<>();
			
			while(true){
			try {
				action = (String) in.readObject();
				//System.out.println(action);
				switch(action){
					case Protocole.USER_LISTE:
						System.out.println("Message du serveur : "+ Protocole.USER_LISTE);
						Integer nb = (Integer) in.readObject();
						for(int i = 0; i<nb; i++){
							String logIn = (String) in.readObject();
							userList.add(new User(logIn));
							Server.jeu.add(new User(logIn));
						}
						/*int index = 0;
						for(User user2 : userList){
							System.out.println(user2.getNom());
							System.out.println(jeu.getUser(index));
							index++;
						}*/
						break;
					case Protocole.USER_CONNECTE:
						System.out.println("Message du serveur : "+ Protocole.USER_CONNECTE);
							user = (User) in.readObject();
							userList.add(user);
							//System.out.println(jeu.getListUsers());
							for(User user1 : userList){
								//System.out.println(user1.getNom());
							}
						break;
					case Protocole.POSITION_BALLE:
						System.out.println("Message du serveur : "+ Protocole.POSITION_BALLE);
						balle = (Balle) in.readObject();
						int balleX = balle.getPosX();
						int balleY = balle.getPosY();
						//jeu.getBalle().setPosX(balleX);
						//jeu.getBalle().setPosX(balleY);
						System.out.println("La position de la balle est "+balleX+" : "+balleY);
						break;
						
					case Protocole.BALLE_TOUCHEE:
						System.out.println("Message du serveur : "+ Protocole.BALLE_TOUCHEE);
						user = (User) in.readObject();
						this.ballesJouees++;
						user.setScore((double) (user.getScore() + 1) / this.ballesJouees);
						break;
						
					case Protocole.BALLE_PERDUE:
						this.ballesJouees++;
						break;
					
					case Protocole.RAQUETTE_DEPLACEE:
						/*System.out.println("Message du serveur : "+ Protocole.RAQUETTE_DEPLACEE);
						user = (User) in.readObject();
						int newRaquettePos = user.getRaquette().getX();
						System.out.println(newRaquettePos);
						break;*/
				}	
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		/*try {
			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
			out.writeObject(Protocole.RAQUETTE_DEPLACEMENT);
			out.writeObject(user);
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		
		
		
		
	}

	public User getUser() {
		return user;
	}
	
}
