package server;

import model_client.Balle;

public class GestionnaireBalle implements Runnable {
	private Server server;
	
	public GestionnaireBalle(Server server) {
		this.server = server;
		Thread threadBall = new Thread(this);
		threadBall.start();
	}
	
	@Override
	public void run() {
		Balle balle = new Balle();
		while (true) {
			//synchronized (this) {
				balle.move();
				server.broadcast(balle, Protocole.POSITION_BALLE);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				
			//}
		}
	}
}
