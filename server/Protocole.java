package server;

public interface Protocole {
	String PSEUDO = "PSEUDO";
	String USER_LISTE = "USERS_LISTE";
	String USER_CONNECTE = "USER_CONNECTE";
	String POSITION_BALLE = "POSITION_BALLE";
	String BALLE_TOUCHEE = "BALLE_TOUCHEE";
	String BALLE_PERDUE = "BALLE_PERDUE";
	String RAQUETTE_DEPLACEMENT = "RAQUETTE_DEPLACEMENT";
	String RAQUETTE_DEPLACEE = "RAQUETTE_DEPLACEE";
	String CLOSE = "CLOSE";
}
