package fr.unicaen.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Collection;

import javax.swing.text.html.HTMLDocument.Iterator;

import fr.unicaen.model.Balle;
import fr.unicaen.model.Jeu;
import fr.unicaen.model.Protocole;
import fr.unicaen.model.Raquette;
import fr.unicaen.model.User;


/**
 * Classe GestionnaireBalle. Gestion de la balle envoyée par le serveur
 * @author
 * @since Février 2016
 *
 */
public class GestionnaireBalle implements Runnable {
	/**
	 * Instance de PrintWriter. Gestion des messages de sortie
	 */
	private PrintWriter out;
	/**
	 * Instance de BufferedReader. Gestion des messages d'entré
	 */
	private BufferedReader in;
	/**
	 * Instance du serveur
	 */
	private MainProcess main;
	
	/**
	 * Constructeur de la classe
	 * @param socket : Socket de la balle
	 * @param main : Instance du serveur
	 */
	public GestionnaireBalle(Socket socket, MainProcess main) {
		this.main = main;
		try {
			out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			new Thread(this).start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Comportement de la balle
	 */
	@Override
	public void run() {
		Balle balle = new Balle();
		 //si l'on recule ou non sur l'axe x
	    boolean backX = false;
	    //si l'on recule ou non sur l'axe y
	    boolean backY = false;
	    int raquetteX;
	    int x = balle.getX();
		int y = balle.getY();
		int balleStartX = balle.getStartX();
		int balleStartY = balle.getStartY();
		int deplacementX = Balle.VITESSE;
		int deplacementY = Balle.VITESSE;
		int widthPan = Jeu.limitWidth-5; //corréction pour le panneau (sur l'autres ordinateurs cela fonctionne bien sans cette correction)
		int heightPan = Jeu.limitHeight-25; //corréction pour le panneau
		
		while (true) {
			for (GestionnaireClient gc : MainProcess.clientList) {
				
				User user = gc.user;
				raquetteX = user.getRaquette().getPosX();
				
				//faire bouger la balle
				if(x < 1) {
//			    	  System.out.println("Left wall touche");
//			    	  System.out.println("Start point " +balle.getStartX()+";"+balle.getStartY()+"");
//			    	  System.out.println("Final point "+balle.getX()+";"+balle.getY());
//			    	  System.out.println();
			    	  backX = false;
			    	  balleStartX = x;
			    	  balleStartY = y;
			      }
			      //Si la coordonnée x est supérieure à la taille du Panneau moins la taille du rond, on recule
			      if(x > widthPan-Balle.DIAMETRE) {
//			    	  System.out.println("Right wall touché");
//			    	  System.out.println("Start point " +balleStartX+";"+balleStartY+"");
//			    	  System.out.println("Final point "+x+";"+y);
//			    	  System.out.println();
			    	  backX = true;
			    	  balleStartX = x;
			    	  balleStartY = y;
			      }
			      //Idem pour l'axe y
			      if(y < 1) {
//			    	  System.out.println("Top wall touché");
//			    	  System.out.println("Start point " +balleStartX+";"+balleStartY+"");
//			    	  System.out.println("Final point "+x+";"+y);
//			    	  System.out.println("With angle "+getCalc(balleStartX, x, balleStartY, y));
//			    	  System.out.println();
			    	  backY = false;
			    	  balleStartX = x;
			    	  balleStartY = y;
			    	  
			      }
			      
			      //la balle est au niveau de la raquette
			      if(y > heightPan-Balle.DIAMETRE-Raquette.HEIGHT) {
			    	  
			    	  int balleCenterX = x+Balle.DIAMETRE/2;
			    	  int partie1FinX = (int) Math.round(raquetteX+Raquette.WIDTH*0.05);
			    	  int partie2FinX = (int) Math.round(partie1FinX+Raquette.WIDTH*0.2);
			    	  int partie3FinX = (int) Math.round(partie2FinX+Raquette.WIDTH*0.2);
			    	  int partie4FinX = (int) Math.round(partie3FinX+Raquette.WIDTH*0.1);
			    	  int partie5FinX = (int) Math.round(partie4FinX+Raquette.WIDTH*0.2);
			    	  int partie6FinX = (int) Math.round(partie5FinX+Raquette.WIDTH*0.2);
			    	  
			    	  //si la raquette est touchée
			    	  if ( (balleCenterX >= raquetteX) && (balleCenterX <= raquetteX+Raquette.WIDTH) ) {
			    		  System.out.println("Raquette touchee");
				    	  System.out.println("Start point " +balleStartX+";"+balleStartY+"");
				    	  System.out.println("Final point "+x+";"+y);
				    	  backY = true;
				    	  
				    	//partie1
				    	  if ( (balleCenterX >= raquetteX) && (balleCenterX <= partie1FinX) ) {
				    		  System.out.println("1e partie de la requette : 175 degrees");
				    		  deplacementX = (int) Math.round((Balle.VITESSE+1)*Math.cos(Math.toRadians(5)));
				    		  deplacementY = (int) Math.round((Balle.VITESSE+1)*Math.sin(Math.toRadians(5)));
				    	  }
				    	  
				    	//partie2
				    	  if ( (balleCenterX > partie1FinX) && (balleCenterX <= partie2FinX) ) {
				    		  System.out.println("2m partie de la requette : 150 degrees");
				    		  deplacementX = (int) Math.round(Balle.VITESSE*Math.cos(Math.toRadians(30)));
				    		  deplacementY = (int) Math.round(Balle.VITESSE*Math.sin(Math.toRadians(30)));
				    	  }
				    	  
				    	//partie3
				    	  if ( (balleCenterX > partie2FinX) && (balleCenterX <= partie3FinX) ) {
				    		  System.out.println("3m partie de la requette : 125 degrees");
				    		  deplacementX = (int) Math.round(Balle.VITESSE*Math.cos(Math.toRadians(65)));
				    		  deplacementY = (int) Math.round(Balle.VITESSE*Math.sin(Math.toRadians(65)));
				    	  }
				    	  
				    	//partie4
				    	  if ( (balleCenterX > partie3FinX) && (balleCenterX <= partie4FinX) ) {
				    		  System.out.println("4m partie de la requette : 90 degrees");
				    		  deplacementX = (int) Math.round(Balle.VITESSE*Math.cos(Math.toRadians(90)));
				    		  deplacementY = (int) Math.round(Balle.VITESSE*Math.sin(Math.toRadians(90)));
				    	  }
				    	  
				    	//partie5
				    	  if ( (balleCenterX > partie4FinX) && (balleCenterX <= partie5FinX) ) {
				    		  System.out.println("5m partie de la requette : 65 degrees");
				    		  deplacementX = (int) Math.round(Balle.VITESSE*Math.cos(Math.toRadians(65)));
				    		  deplacementY = (int) Math.round(Balle.VITESSE*Math.sin(Math.toRadians(65)));
				    	  }
				    	  
				    	//partie6
				    	  if ( (balleCenterX > partie5FinX) && (balleCenterX <= partie6FinX) ) {
				    		  System.out.println("6m partie de la requette : 30 degrees");
				    		  deplacementX = (int) Math.round(Balle.VITESSE*Math.cos(Math.toRadians(30)));
				    		  deplacementY = (int) Math.round(Balle.VITESSE*Math.sin(Math.toRadians(30)));
				    	  }
				    	  
				    	//partie7
				    	  if ( (balleCenterX > partie6FinX) && (balleCenterX <= (raquetteX+Raquette.WIDTH)) ) {
				    		  System.out.println("7m partie de la requette : 5 degrees");
				    		  deplacementX = (int) Math.round((Balle.VITESSE+1)*Math.cos(Math.toRadians(5)));
				    		  deplacementY = (int) Math.round((Balle.VITESSE+1)*Math.sin(Math.toRadians(5)));
				    	  }
				    	  
				    	  System.out.println("dx="+deplacementX+" dy="+deplacementY);
				    	  balleStartX = x;
				    	  balleStartY = y;
				    	  
				    	  //calculer le score d'utilisateur qui a touché la balle
				    	  user.setScore(user.getScore()+MainProcess.clientList.size());
				    	  
							synchronized (this) {
								for (GestionnaireClient c : MainProcess.clientList) {
									c.out.println(Protocole.BALLE_TOUCHEE);
									c.out.println(MainProcess.clientList.size());
									for (GestionnaireClient client : MainProcess.clientList) {
										c.out.println(client.user.getNom());
										c.out.println(client.user.getScore());
									}
									c.out.flush();
								}
							}
			    	  }
			    	  
			    	  
			    	  
			    	  if(y > heightPan-Balle.DIAMETRE) {
			    		  System.out.println("Balle est perdue");
			    		  x = 0;
			    		  y = 0;
			    		  synchronized (this) {
								for (GestionnaireClient c : MainProcess.clientList) {
									c.user.prochaineEssai();
									c.out.println(Protocole.BALLE_PERDUE);
									c.out.flush();
								}
							}
			    	  }
			    	  
			    	  System.out.println();
			      }
			      
			      //Si on n'avance pas, on decrémente la coordonnée
			      if(backX) {
			    	  x -= deplacementX;
			    	  balle.setX(x);
			      }
			      //Sinon, on incrémente
			      else{
			    	  x += deplacementX;
			    	  balle.setX(x);
			      }
			      //Idem pour l'axe Y
			      if(backY) {
			    	  y -= deplacementY;
			    	  balle.setY(y);
			      }
			      else {
			    	  y += deplacementY;
			    	  balle.setY(y);
			      }
				
				
					synchronized (main) {
						for (GestionnaireClient c : MainProcess.clientList) {
							c.out.println(Protocole.POSITION_BALLE);
							c.out.println(balle.getX());
							c.out.println(balle.getY());
							c.out.flush();
						}
					}
					try {
						Thread.sleep(5);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
			}
		}
	}
	

	/**
	 * Pour calculer l'angle avec lequel la balle touche le mur/la raquette
	 * @param x1 start x de point d'où vient la balle
	 * @param x2 fin x de point où la balle a touché le mur/la raquette, où elle est arrivée
	 * @param y1 start y
	 * @param y2 fin y
	 * @return degrees Angle
	 */
	public double getCalc(int x1, int x2, int y1, int y2) {
	    double deltaX = Math.abs(x2-x1);
	    double deltaY = Math.abs(y2-y1);
	    double degrees = Math.toDegrees((java.lang.Math.atan2(deltaX, deltaY)));

	    return degrees;
	}
}
