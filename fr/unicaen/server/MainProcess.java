package fr.unicaen.server;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Classe Serveur
 * @author
 * @since Février 2016
 *
 */
public class MainProcess {
	
	/**
	 * Socket du serveur
	 */
	private ServerSocket serveurSocket;

	/**
	 * Liste d'utilisateurs connectés
	 */
	
	public static ArrayList<GestionnaireClient> clientList = new ArrayList<GestionnaireClient>();
	
	/**
	 * Méthode pour lancer le Serveur
	 */
	public void lancerServeur() {
		try {
			serveurSocket = new ServerSocket(5555);
			
			//Le serveur attend une connexion et il lance le gestionnaire client pour gérer le theard 
			//de chaque utilisateur connecté
			while (true) {
				System.out.println("Attente de clients : accept()");
				Socket socket = serveurSocket.accept();
				System.out.println("accept() est debloqu� : client connect�");
				this.clientList.add(new GestionnaireClient(socket,this));
				
				// Getion de thread de la balle
				System.out.println("Balle cree");
				new GestionnaireBalle(socket, this);
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
		
	/**
	 * Main. Démarrage du serveur
	 * @param args
	 */
	public static void main(String[] args) {
		new MainProcess().lancerServeur();
	}
	
}
