package fr.unicaen.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Collection;

import fr.unicaen.model.Protocole;
import fr.unicaen.model.User;

/**
 * Classe GestionnaireClient : gestion du thread de chaque client connecté 
 * et les messages à envoyer 
 * @author
 * @since Février 2016
 */
public class GestionnaireClient implements Runnable {
	
	/**
	 * Instance de PrintWriter pour gérer les messages sortants
	 */
	public PrintWriter out;
	
	/**
	 * Instance de BufferedReader pour gérer les messages entrants 
	 */
	public BufferedReader in;
	
	/**
	 * Le pseudo de connexion de l'utilisateur 
	 */
	private String pseudo;
	
	/**
	 * Instance du serveur
	 */
	private MainProcess main;
	
	/**
	 * Création d'un instance d'utilisateur  
	 */
	public User user = new User("init");
	
	/**
	 * Constructeur de la classe
	 * @param s Socket client
	 * @param main Socket du serveur
	 */
	public GestionnaireClient(Socket s, MainProcess main) {
		this.main = main;
		try {
			out = new PrintWriter(new OutputStreamWriter(s.getOutputStream()));
			in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			new Thread(this).start();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	@Override
	public void run() {
		String inputMsg;
		while (true) {
			try {
				inputMsg = in.readLine();
				switch (inputMsg) {
				// Récupération du pseudo et envoi au serveur
				case Protocole.PSEUDO:
					System.out.println("Pseudo est capte");
					synchronized (main) {
						this.pseudo = in.readLine();
						this.user.setNom(this.pseudo);
						out.println(Protocole.USERS_LISTE);
						out.println(MainProcess.clientList.size());
						for (GestionnaireClient c : MainProcess.clientList) {
							out.println(c.user.getNom());
							out.println(c.user.getScore());
							out.println(c.user.getEssai());
							System.out.println("score of user "+user+" "+c.user.getScore());
							if (this != c) {
								c.out.println(Protocole.USER_CONNECTE);
								c.out.println(this.pseudo);
								c.out.flush();
							}
						}
						out.flush();

					}
					break;
				// Envoi de la position de la raquette	
				case Protocole.RAQUETTE_DEPLACEMENT:
					synchronized (main) {
						int  raquettePosX = Integer.parseInt(in.readLine());
						this.user.getRaquette().setPosX(raquettePosX);
//						System.out.println("Raquette est bougee par "+this.pseudo+" raquette pos selon x est " + raquettePosX );
						for (GestionnaireClient c : MainProcess.clientList) {
							if (this != c) {
								c.out.println(Protocole.RAQUETTE_DEPLACEE);
								c.out.println(pseudo);
								c.out.println(raquettePosX);
								c.out.flush();
							}
						}
					}
					break;

				/*
				 * case Protocole.ECRIRE_COLLECTIF: inputMsg = in.readLine();
				 * main.broadcast(pseudo, inputMsg); break; case
				 * Protocole.ECRIRE_PRIVEE: String receiver = in.readLine();
				 * inputMsg = in.readLine(); main.emit(pseudo, inputMsg,
				 * receiver); break;
				 */
				default:
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
			}
		}

	}

	/*public void envoyerMsg(String typeDeLecture, String pseudo, String msg) {
		synchronized (main) {
			out.println(typeDeLecture);
			out.println(pseudo);
			out.println(msg);
			out.flush();
		}
	}*/

}
