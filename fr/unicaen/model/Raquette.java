package fr.unicaen.model;

import java.awt.Color;
import java.io.Serializable;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import fr.unicaen.simple_change.SimpleChangeModel;

/**
 * Classe Raquette. Modèle écoutable
 * @since Février 2016
 */
public class Raquette extends SimpleChangeModel {
	
	/**
	 * Position de la raquette dans l'axe x
	 */
	private int posX;
	/**
	 * Postition de la raquette dans l'axe y
	 */
	private int posY;
	/**
	 * Couleur de la raquette
	 */
	private Color color;
	/**
	 * Hauteur par défaut de la raquette
	 */
	public static final int HEIGHT = 20;
	/**
	 *  Largeur par défaut de la raquette
	 */
	public static final int WIDTH = 100;
	
	/**
	 * Constructeur de la classe. Avec la position par défaut
	 * 
	 */
	public Raquette() {
		this.posX = 50;
		this.posY = 100;
	}
	
	/*
	 * Getters & Setters
	 */
	
	public int getPosX() {
		return posX;
	}


	public void setPosX(int posX) {
		this.posX = posX;
		fireSimpleChange();
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}
	
	@Override
	public String toString() {
		return "raquette position "+this.posX+";"+this.posY;
	}
	
	public Color getColor() {
		return color;
	}
	
	public void setColor() {
		this.color = new Color(randInt(0,255),randInt(0,255),randInt(0,255),100);
	}
	/**
	 * Méthode random pour gérer la couleur de la raquette 
	 * @param min : valeur minimale
	 * @param max : valeur maximale
	 * @return nombre aléatoire
	 */
	public static int randInt(int min, int max) {
	    return ThreadLocalRandom.current().nextInt(min, max + 1);
	}

}
