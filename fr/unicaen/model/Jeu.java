package fr.unicaen.model;

import java.util.ArrayList;
import java.util.List;

import fr.unicaen.simple_change.SimpleChangeListener;
import fr.unicaen.simple_change.SimpleChangeModel;
/**
 * Classe Jeu. Modèle du salle du jeu
 * @author
 * @since Février 2016
 *
 */
public class Jeu extends SimpleChangeModel implements SimpleChangeListener {
	
	/**
	 * Instance de la balle
	 */
	private Balle balle;
	/**
	 * Utilisateur courant
	 */
	private User clientCourant;
	/**
	 * Liste des utilisateurs
	 */
	private List<User> listUsers;
	/**
	 * Largeur du tableau du jeu
	 */
	public static final int limitWidth = 800;
	/**
	 * Hauteur du salle du jeu
	 */
	public static final int limitHeight = 700;
	
	/**
	 * Nombre d'echanges dans chaque partie de jeu
	 */
	public static int nombreEchange = 0;
	
	/**
	 * Constructeur de la classe. Le jeu es composé d'une balle et d'une liste d'utilisateurs
	 * (joueurs)
	 */
	public Jeu() {
		this.listUsers = new ArrayList<User>();
		this.balle = new Balle();
	}
	/*
	 	Getters & Setters
	 */
	public Balle getBalle() {
		return balle;
	}

	public void setBalle(Balle balle) {
		this.balle = balle;
		fireSimpleChange();
	}
	
	public void setClientCourant(User user) {
		this.clientCourant = user;
	}
	/**
	 * Ajout d'un utilisateur à la liste 
	 * @param u Instance de User
	 */
	public void add(User u) {
		u.getRaquette().setColor();
		this.listUsers.add(u);
		u.addSimpleChangeListener(this);
		fireSimpleChange();
//		System.out.println("stateChanged from model");
//		System.out.println("users size is "+this.listUsers.size());
	}
	/**
	 * Suppression d'un utilisateur de la liste
	 * @param u
	 */
	public void remove(User u) {
		this.listUsers.remove(u);
		u.removeSimpleChangeListener(this);
	}
	/**
	 * Trouver un utilisateur dans la liste avec l'index
	 * @param index 
	 * @return
	 */
	public User getUser(int index) {
		return this.listUsers.get(index);
	}
	
	public User getUserParPseudo(String pseudo) {
//		System.out.println("getUserParPseudo "+pseudo);
		for (User user : this.listUsers) {
//			System.out.println("check user with name "+user.getNom());
//			System.out.println(pseudo.equals(user.getNom()));
			if (pseudo.equals(user.getNom())) {
//				System.out.println("User found "+user);
				return user;				
			}
		}
		return null;
	}

	@Override
	public void stateChanged(Object source) {
//		System.out.println("Jeu model stateChanged() "+source.getClass());
		fireSimpleChange();
	}

	public List<User> getListUsers() {
		return listUsers;
	}

	public void setListUsers(List<User> listUsers) {
		this.listUsers = listUsers;
		fireSimpleChange();
	}
	
	public int getUsersCount() {
		return listUsers.size();
	}

	public void changeUserInfo(User user) {
		for (User userItem : listUsers) {
			if (userItem.getNom() == user.getNom()) {
				userItem = user;
			}
		}
	}

	public User getClientCourant() {
		if (this.getUserParPseudo(this.clientCourant.getNom()) != null)
			this.clientCourant = this.getUserParPseudo(this.clientCourant.getNom());
		return this.clientCourant;
	}
	
}
