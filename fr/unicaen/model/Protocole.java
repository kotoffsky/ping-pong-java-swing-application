package fr.unicaen.model;

/**
 * Interface Protocole. Gestion des actions pour la communication entre le serveur,
 * la balle et les clients
 * @author 
 * @since Février 
 */
public interface Protocole {
	 String PSEUDO = "PSEUDO";
	 String USERS_LISTE = "USERS_LISTE";
	 String USER_CONNECTE = "USER_CONNECTE";
	 String RAQUETTE_DEPLACEMENT = "RAQUETTE_DEPLACEMENT";
	 String RAQUETTE_DEPLACEE = "RAQUETTE_DEPLACEE";
	 String POSITION_BALLE = "POSITION_BALLE";
	 String BALLE_TOUCHEE = "BALLE_TOUCHEE";
	 String BALLE_PERDUE = "BALLE_PERDUE";
}
