package fr.unicaen.model;

import fr.unicaen.server.GestionnaireBalle;
import fr.unicaen.simple_change.SimpleChangeListener;
import fr.unicaen.simple_change.SimpleChangeModel;

/**
 * Classe User, Modèle écoutable d'un utilisateur connecté
 * @since Février 2016
 */
public class User extends SimpleChangeModel implements SimpleChangeListener {
	/**
	 * id de l'utilisateur
	 */
	private int id;
	/**
	 * Score de l'utilisateur
	 */
	private double score;
	/**
	 * Nombre de balles
	 */
	private int essai;
	/**
	 * Pseudo (nom) de l'utilisateur
	 */
	private String nom;
	/**
	 * Raquette de l'utilisateur
	 */
	private Raquette raquette;
	
	/**
	 * Constructeur de la classe. On abonne la raquette pour qu'elle soit écoutée 
	 * @param nom : On crée l'utilistatur avec le pseudo.
	 */
	public User(String nom) {
		this.essai = 1;
		this.score = 0;
		this.nom = nom;
		this.raquette = new Raquette();
		this.raquette.addSimpleChangeListener(this);
	}
	/*
	 * Getters & Setters
	 */
	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
		fireSimpleChange();
	}

	public Raquette getRaquette() {
		return raquette;
	}

	public void setRaquette(Raquette raquette) {
		this.raquette = raquette;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public int getEssai() {
		return this.essai;
	}
	
	public void setEssai(int essai) {
		this.essai = essai;
	}
	
	/**
	 * On augment le nombre d'essais (balles jouées)
	 */
	public void prochaineEssai() {
		this.essai++;
		fireSimpleChange();
		System.out.println(this + " ProchaineEssaie "+this.essai);
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", score=" + score + ", nom=" + nom + ", raquette=" + raquette + "] essaie="+this.essai;
	}
	/**
	 * On previent que la raquette de l'utilisateur à changée
	 */
	@Override
	public void stateChanged(Object source) {
		fireSimpleChange();
	}
	
}
