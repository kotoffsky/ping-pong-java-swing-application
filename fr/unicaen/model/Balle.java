package fr.unicaen.model;

import java.awt.Color;
import java.io.Serializable;

import fr.unicaen.simple_change.SimpleChangeModel;
/**
 * Classe Balle. Modèle de la Balle
 * @Since Février 2016
 */
public class Balle extends SimpleChangeModel implements Serializable{
	/**
	 * Position de départ
	 */
	private int startX=100,startY=100;
	/**
	 * 
	 */
	private int x=100,y=100;
	/**
	 * Taille de la balle 
	 */
	public final static int DIAMETRE = 20;
	/**
	 * Couleur par défaut (rouge)
	 */
	public final static Color COULEUR = Color.red;
	/**
	 * Vitesse par défaut
	 */
	public final static int VITESSE = 4;
	
	public static boolean inverse = false;
	/**
	 * Déplacement par défaut  dans l'axe x
	 */
	private int deplacementX = 1;
	/**
	 * Déplacement par défaut dans l'axe y
	 */
	private int deplacementY = 1;
	
	public Balle() {
		
	}
	
	/**
	 Getters & Setters
	 **/
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
		fireSimpleChange();
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
		fireSimpleChange();
	}
	
	public int getStartX() {
		return startX;
	}
	
	public void setStartX(int startX) {
		this.startX = startX;
		
	}
	
	public int getStartY() {
		return startY;
	}
	
	public void setStartY(int startY) {
		this.startY = startY;
	}
	
	public int getDeplacementX() {
		return deplacementX;
	}
	
	public void setDeplacementX(int deplacementX) {
		this.deplacementX = deplacementX;
	}
	
	public int getDeplacementY() {
		return deplacementY;
	}
	
	public void setDeplacementY(int deplacementY) {
		this.deplacementY = deplacementY;
	}

	@Override
	public String toString() {
		return "Balle [startX=" + startX + ", startY=" + startY + ", x=" + x + ", y=" + y + ", deplacementX="
				+ deplacementX + ", deplacementY=" + deplacementY + "]";
	}
	
	
}
