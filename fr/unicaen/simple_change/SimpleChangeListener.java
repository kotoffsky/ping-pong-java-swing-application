package fr.unicaen.simple_change;

public interface SimpleChangeListener {
	void stateChanged(Object source);
}