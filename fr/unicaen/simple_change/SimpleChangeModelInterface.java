package fr.unicaen.simple_change;

public interface SimpleChangeModelInterface {
	void addSimpleChangeListener(SimpleChangeListener listener);
	void removeSimpleChangeListener(SimpleChangeListener listener);
}
