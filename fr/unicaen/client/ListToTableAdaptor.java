package fr.unicaen.client;

import java.text.DecimalFormat;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import fr.unicaen.model.Jeu;
import fr.unicaen.model.User;
import fr.unicaen.simple_change.*;

public class ListToTableAdaptor extends AbstractTableModel implements SimpleChangeListener {
	
	private List<User> listUsers;
	private final static int NOM = 0;
	private final static int ESSAIE = 1;
	private final static int SCORE = 2;
	private final static int POURCENTAGE = 3;
	private final static int NB_COLONNES = 4;
	
	public ListToTableAdaptor(Jeu jeu){
		this.listUsers = jeu.getListUsers();
		jeu.addSimpleChangeListener(this);
	}
	@Override     
	public int getRowCount() {
		return listUsers.size();
	}

	@Override
	public int getColumnCount() {
		return NB_COLONNES;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		User user = listUsers.get(rowIndex);
		switch(columnIndex)
		{
		case NOM:
			return user.getNom();
		case ESSAIE:
			return user.getEssai();
		case SCORE:
			return user.getScore();
		case POURCENTAGE:
			Double totalScore = 0.0;
			for (User userItem : listUsers) {
				totalScore += userItem.getScore();
			}
			Double relative = user.getScore()/totalScore*100;
			return Math.round(relative*100) / 100;
		}
		return null;
	}
	/**
	 * Affichage des labels des champs du tableau
	 */
	@Override
	public String getColumnName(int columnIndex) {
		switch(columnIndex)
		{
		case NOM:
			return "Nom";
		case ESSAIE:
			return "Essai";
		case SCORE:
			return "Score";
		case POURCENTAGE:
			return "Pourcentage";
		}
		return null;
	}
	
	@Override
	public void stateChanged(Object source) {
		fireTableDataChanged();		
	}
}
