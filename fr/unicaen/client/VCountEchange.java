package fr.unicaen.client;

import javax.swing.JLabel;

import fr.unicaen.model.Jeu;
import fr.unicaen.simple_change.SimpleChangeListener;

/**
 * Classe VCountEchage, elle écoute le nombre d'échanges pour les afficher
 * @since Février 2016
 *
 */
public class VCountEchange extends JLabel implements SimpleChangeListener{
	
	private Jeu jeuModel;
	/**
	 * Constructeur de la classe.
	 * @param jeuModel : Modèle de la salle de jeu
	 */
	public VCountEchange(Jeu jeuModel)
	{
		this.setText("Nombre d'échanges : " + 0);
		this.jeuModel = jeuModel;
		jeuModel.addSimpleChangeListener(this);
	}

	/**
	 * affiche le nombre d'échanges
	 */
	@Override
	public void stateChanged(Object source) {
		this.setText("Nombre d'échanges : " + Jeu.nombreEchange);
		
	}
}
