package fr.unicaen.client;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Class VueConnexion, c'est une vue-controleur qui permet la connexion d'un utilisateur
 * On capte l'événement quand on appuie sur le button de connexion 
 * @since Février 2016
 *
 */
public class VueConnexion extends JPanel implements ActionListener{
	
	/**
	 * Champ de texte éditable
	 */
	private JTextField jtf;
	/**
	 * Instance de JButton
	 */
	private JButton btnConnexion;
	/**
	 * Instance de l'ensemble du jeu
	 */
	private PongGUI pong;
	
	/**
	 * Constructeur de la Classe.
	 * @param pong : Instance de l'ensemble du jeu
	 */
	public VueConnexion(PongGUI pong) {
		this.pong = pong;
		this.setLayout(new FlowLayout());
		
		jtf = new JTextField();
		jtf.setPreferredSize(new Dimension(150, 26));
		btnConnexion = new JButton("Connexion");
		btnConnexion.addActionListener(this);
		this.add(jtf);
		this.add(btnConnexion);
	}
	
	/**
	 * On récupère le pseudo entré dans le JTextFild  
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("Button clicked. Text is "+jtf.getText());
		this.pong.startConnexion(jtf.getText());
		this.setVisible(false);
		
	}
}
