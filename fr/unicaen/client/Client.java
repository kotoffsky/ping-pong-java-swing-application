package fr.unicaen.client;

import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.ListModel;

import fr.unicaen.model.Jeu;
import fr.unicaen.model.Protocole;
import fr.unicaen.model.User;

/**
 * Classe Client. Contrôleur qui reçoit les messages du serveur et qui fait le lien 
 * avec la partie client
 * @since Février 2016
 */
public class Client {

	//ArrayList<String> listData;
	//private JFrame salle;
	//public static JList<String> userList;
	/**
	 * Instance du jeu
	 */
	private Jeu jeuModel;
	/**
	 * Socket de connexion client
	 */
	private Socket socket = null;
	/**
	 * Instance de BufferedReader pour gérer les message d'entrée
	 */
	private BufferedReader in = null;
	/**
	 * Instance de PrintWriter pour gérer les messages de sortie
	 */
	public static PrintWriter out = null;
	/**
	 * Utiliateur courant (celui qui joue)
	 */
	private User userCourant;

	/**
	 * Constructeur de la Classe
	 * @param jeuModel
	 */
	public Client(Jeu jeuModel) {
		this.jeuModel = jeuModel;
		this.userCourant = this.jeuModel.getClientCourant();
		init();
	}
	/**
	 * Initialisation d'un client une fois connecté (user - joueur)
	 */
	public void init() {
		
		synchronized (this) {
			
			while (this.jeuModel.getClientCourant().getNom().equals("")) {
				System.out.println("wait for user");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

		try {
			// Connexion au Serveur dans le localhost de la machine pour le moment 
			socket = new Socket("localhost", 5555);
			this.in = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			this.out = new PrintWriter(new OutputStreamWriter(
					socket.getOutputStream()));
			out.println(Protocole.PSEUDO);
			out.println(this.userCourant.getNom());
			out.flush();
			while (true) {
				String motCle = in.readLine();
				switch (motCle) {
				// Réception du nombre du Thread-Clients (gestionnaireClient) et du pseudo pour créer 
				//une nouvelle instance d'un User
					case Protocole.USERS_LISTE:
						System.out.println("Message du serveur : " + Protocole.USERS_LISTE);
						String nb = in.readLine();
						System.out.println("Client side : user number is "+nb+ "ses nomes");
						for (int i = 0; i < Integer.parseInt(nb); i++) {
							User newUser = new User(in.readLine());
							newUser.setScore(Double.parseDouble(in.readLine()));
							newUser.setEssai(Integer.parseInt(in.readLine()));
							System.out.println("From list "+newUser);
							this.jeuModel.add(newUser);
						}
						break;
					//Ajout du nouvel utilisateur(joueur) à la liste de jeu	
					case Protocole.USER_CONNECTE:
						System.out.println("Message du serveur : "
								+ Protocole.USER_CONNECTE);
	
						String pseudo = in.readLine();
						this.jeuModel.add(new User(pseudo));
						System.out.println(pseudo);
						break;
					// Réception des positions des raquettes des autres utilisaturs	
					case Protocole.RAQUETTE_DEPLACEE:
						System.out.println("Message du serveur : "
								+ Protocole.RAQUETTE_DEPLACEE);
//						System.out.println(this.jeuModel.getUserParPseudo(in.readLine()).getNom());
						this.jeuModel.getUserParPseudo(in.readLine()).getRaquette().setPosX(Integer.parseInt(in.readLine()));
						break;
					//Réception de la position de la balle	
					case Protocole.POSITION_BALLE:
//						System.out.println("Message du serveur : " + Protocole.POSITION_BALLE);
						this.jeuModel.getBalle().setX(Integer.parseInt(in.readLine()));
						this.jeuModel.getBalle().setY(Integer.parseInt(in.readLine()));
						break;
					//	Identification de l'utilisateur qui a touché la balle
					case Protocole.BALLE_TOUCHEE:
						System.out.println("Message du serveur : " + Protocole.BALLE_TOUCHEE);
						Jeu.nombreEchange++;
						String nbUsers = in.readLine();
						for (int i = 0; i < Integer.parseInt(nbUsers); i++) {
							this.jeuModel.getUserParPseudo(in.readLine()).setScore(Double.parseDouble(in.readLine()));
						}
						break;
					// on augmente le nombre d'essais de chaque utilisateur si la balle tombe	
					case Protocole.BALLE_PERDUE:
						System.out.println("Message du serveur : " + Protocole.BALLE_PERDUE);
						Jeu.nombreEchange = 0;
						for (User user : this.jeuModel.getListUsers()) {
							user.prochaineEssai();
//							System.out.println(user.getEssaie());
						}
						break;
				}
			}

		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
