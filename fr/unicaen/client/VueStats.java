package fr.unicaen.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import fr.unicaen.model.Jeu;
import fr.unicaen.model.User;
import fr.unicaen.simple_change.SimpleChangeListener;
/**
 * Classe VueStats. Vue pour afficher la liste de joueurs, leur score et leurs stats 
 * @since Févirer 2016
 *
 */
public class VueStats extends JPanel {
	
	public VueStats(Jeu jeuModel){
		this.setLayout(new BorderLayout());
		
		TableUsers table = new TableUsers(jeuModel);
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBackground(Color.LIGHT_GRAY);
		scrollPane.setPreferredSize(new Dimension(300, Jeu.limitHeight-50));
		scrollPane.revalidate();
		add(scrollPane, BorderLayout.CENTER);
		JPanel count = new JPanel();
		count.setLayout(new GridLayout(2,1));
		count.add(new VCountEchange(jeuModel));
		count.add(new VCountBalle(jeuModel));
		this.add(count, BorderLayout.SOUTH);
//		this.add(new VCountBalle(jeuModel), BorderLayout.SOUTH);
//		this.setSize(150, Jeu.limitHeight);
	}

}