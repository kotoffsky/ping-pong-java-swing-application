package fr.unicaen.client;

import javax.swing.JLabel;

import fr.unicaen.model.Jeu;
import fr.unicaen.model.User;
import fr.unicaen.simple_change.SimpleChangeListener;

/**
 * Classe VCountBalle. Elle écoute les balles joués pour chaque utilisateur
 * @since Février 2016
 *
 */
public class VCountBalle extends JLabel implements SimpleChangeListener{
	private Jeu jeuModel;
	
	/**
	 * Constructeur de la classe
	 * On écoute le modèle
	 * @param jeuModel : Modèle de la salle du jeu
	 */
	public VCountBalle(Jeu jeuModel)
	{
		this.jeuModel = jeuModel;
		this.setText("Nombre de balles : " + 0);
		jeuModel.addSimpleChangeListener(this);
	}
	
	/**
	 * On affiche l'utilisateur et le nombre de balles jouées  
	 */
	@Override
	public void stateChanged(Object source) {
		this.setText("Nombre de balles "+ this.jeuModel.getClientCourant().getEssai());
	}
}
