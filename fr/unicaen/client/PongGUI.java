package fr.unicaen.client;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.JFrame;

import fr.unicaen.model.Jeu;
import fr.unicaen.model.User;

/**
 * Classe PongGUI. Vue globale du jeu
 * @since Février 2016
 *
 */
public class PongGUI extends JFrame {
	public Jeu jeuModel;
	public PongGUI() {
		jeuModel = new Jeu();
		jeuModel.setClientCourant(new User(""));
		
		VueJeu jeuVue = new VueJeu(jeuModel);
		VueStats statsVue = new VueStats(jeuModel);
		statsVue.add(new VueConnexion(this),BorderLayout.NORTH);
		
		Container container = getContentPane();
		container.setLayout(new BorderLayout());
		container.add(jeuVue, BorderLayout.CENTER);
		container.add(statsVue, BorderLayout.EAST);
		
//		this.pack();
		System.out.println("Stats vue width is "+statsVue.getSize().width);
		this.setTitle("Super pong !");
		this.setSize(Jeu.limitWidth+300,Jeu.limitHeight);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.setResizable(false);
		
		new Client(this.jeuModel);
	}
	
	/**
	 * Modification du pseudo à partir d'une connexion pour changer le pseudo 
	 * du client précédemment crée (initialisé à vide)  
	 * @param pseudo
	 */
	public void startConnexion(String pseudo) {
		this.jeuModel.getClientCourant().setNom(pseudo);
	}
	
}
