package fr.unicaen.client;

import java.awt.Color;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

import fr.unicaen.model.Balle;
import fr.unicaen.model.Jeu;
import fr.unicaen.model.Protocole;
import fr.unicaen.model.Raquette;
import fr.unicaen.model.User;
import fr.unicaen.simple_change.SimpleChangeListener;
/**
 * Classe VueJeu. Vue - contrôleur pour le modèle de la salle de jeu.
 * Ici, on écoute le mouvement de la souris pour bouger la raquette et le jeu même. 
 * @since Février 2016
 *
 */
public class VueJeu extends JPanel implements SimpleChangeListener, MouseMotionListener{
	
	/**
	 * Instance de la salle de jeu
	 */
	private Jeu jeuModel;
	/**
	 * Utilisateur courant, celui qui joue
	 */
	private User clientCourant;
	
	//public ArrayList<User> usersList = new ArrayList<User>();
	/**
	 * Intance de la balle
	 */
	private Balle balle;
	
	/**
	 * Constructeur de la classe. On abonne le jeu et le mouvement de la souris pour qu'il soient 
	 * écoutables.
	 * @param jeuModel Instance du jeu
	 */
	public VueJeu(Jeu jeuModel)
	{
		this.jeuModel = jeuModel;
		this.jeuModel.addSimpleChangeListener(this);
		this.balle = this.jeuModel.getBalle();
		this.balle.addSimpleChangeListener(this);
		this.clientCourant = this.jeuModel.getClientCourant();
		this.addMouseMotionListener(this);
	}
	
	/**
	 * On dessine la balle et la raquette
	 */
	@Override
	public void paintComponent(Graphics g)
	{
			g.setColor(Color.white);
			g.fillRect(0, 0, this.getWidth(), this.getHeight());
			g.setColor(Balle.COULEUR);
			g.fillOval(this.balle.getX(), 
					this.balle.getY(), 
					Balle.DIAMETRE, 
					Balle.DIAMETRE);
			
			g.setColor(Color.GREEN);
			g.drawLine(0, this.getHeight()-Raquette.HEIGHT-Balle.DIAMETRE, this.getWidth(), this.getHeight()-Raquette.HEIGHT-Balle.DIAMETRE);
			
			for (User user : this.jeuModel.getListUsers()) {
				if (!user.getNom().equals(this.clientCourant.getNom())) {
					g.setColor(user.getRaquette().getColor());
		            g.fillRect(user.getRaquette().getPosX(),
		            		this.getHeight()-Raquette.HEIGHT,
		            		Raquette.WIDTH,
		    				Raquette.HEIGHT);
				}
			}
			
			g.setColor(Color.blue);
			g.fillRect(this.clientCourant.getRaquette().getPosX(), 
					this.getHeight() - Raquette.HEIGHT,
					Raquette.WIDTH,
					Raquette.HEIGHT);
	}
	/**
	 * On redessine les éléments (balle, raquette) quand ils bougent
	 */
	@Override
	public void stateChanged(Object source) {
		repaint();
	}
	/**
	 * On capte l'événement (déplacement) de la souris pour faire bouger la raquette
	 */
	@Override
	public void mouseMoved(MouseEvent arg0) {
		synchronized (this) {
			if (Client.out != null && this.clientCourant!=null && ( arg0.getX() < (this.getWidth() - this.clientCourant.getRaquette().WIDTH))) {
				this.clientCourant.getRaquette().setPosX(arg0.getX());
				Client.out.println(Protocole.RAQUETTE_DEPLACEMENT);
				Client.out.println(this.clientCourant.getRaquette().getPosX());
				Client.out.flush();
				repaint();
			}
		}
	}

	//rien pour le drag de la souris
	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
