package fr.unicaen.client;

import javax.swing.JTable;

import fr.unicaen.model.Jeu;

/**
 * Classe TableUsers. Utilistion d'un adapteur pour afficher la liste des utilisateurs et leurs stats 
 * dans JTable
 * @since Février 2016
 *
 */
public class TableUsers extends JTable {

	public TableUsers(Jeu jeu)
	{
		super(new ListToTableAdaptor(jeu));
	}
}
