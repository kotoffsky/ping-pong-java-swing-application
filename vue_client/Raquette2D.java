package vue_client;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import model_client.Jeu;

import SimpleChange.SimpleChangeListener;

public class Raquette2D extends Vue2D {
	private final static int HEIGHT = 20;
	private final static int WIDTH = 100;
	private int posX, posY;
	private VueJeu jeu;

	public Raquette2D(Color color) {
		super(HEIGHT, WIDTH, color);
		this.posX = 580;
		this.posY = 700;
	}
	
	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX, VueJeu j) {
		this.posX = posX-this.width/2;
		this.jeu=j;
		stateChanged(this);
	}

	public int getPosY() {
		return posY;
	}
	
	@Override
	public void paintComponent(Graphics g2d) {
		g2d.fillRect(posX, posY, WIDTH, HEIGHT);
		g2d.setColor(color);
	}
	

	@Override
	public void draw(Graphics2D g2d) {
		g2d.fillRect(posX, posY, WIDTH, HEIGHT);
		g2d.setColor(color);
		paint(g2d);
	}

	@Override
	public void stateChanged(Object source) {
		jeu.repaint();
	}
}
