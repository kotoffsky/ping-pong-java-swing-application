package vue_client;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import SimpleChange.*;
import model_client.Jeu;
import model_client.User;

public class ListToTableAdaptor extends AbstractTableModel implements SimpleChangeListener {
	
	private List<User> listUsers;
	private final static int NOM = 0;
	private final static int SCORE = 1;
	private final static int NB_COLONNES = 2;
	
	public ListToTableAdaptor(Jeu jeu){
		this.listUsers = jeu.getListUsers();
		jeu.addSimpleChangeListener(this);
	}
	@Override     
	public int getRowCount() {
		return listUsers.size();
	}

	@Override
	public int getColumnCount() {
		return NB_COLONNES;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		User user = listUsers.get(rowIndex);
		switch(columnIndex)
		{
		case NOM:
			return user.getNom();
		case SCORE:
			return user.getScore();
		}
		return null;
	}
	
	@Override
	public String getColumnName(int columnIndex) {
		switch(columnIndex)
		{
		case NOM:
			return "Nom";
		case SCORE:
			return "Score";
		}
		return null;
	}
	
	@Override
	public void stateChanged(Object source) {
		fireTableDataChanged();		
	}
}
