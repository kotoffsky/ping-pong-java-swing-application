package vue_client;

import javax.swing.JTable;

import model_client.Jeu;

public class TableUsers extends JTable {

	public TableUsers(Jeu jeu)
	{
		super(new ListToTableAdaptor(jeu));
	}
}
