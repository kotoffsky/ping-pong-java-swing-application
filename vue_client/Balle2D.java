package vue_client;

import java.awt.Color;
import java.awt.Graphics2D;

import model_client.Balle;

public class Balle2D extends Vue2D {
	private final static int HEIGHT = 20;
	private final static int WIDTH = 20;
	//private int posX, posY;
	private Balle balle;

	public Balle2D(Color color) {
		super(HEIGHT, WIDTH, color);
		balle = new Balle();
	}

	/*public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}*/

	@Override
	public void stateChanged(Object source) {
		repaint();
	}

	@Override
	public void draw(Graphics2D g2d) {
		g2d.fillOval(balle.getPosX(), balle.getPosY(), WIDTH, HEIGHT);
		g2d.setColor(color);
		paint(g2d);
	}	
}
