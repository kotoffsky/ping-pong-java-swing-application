package vue_client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;

import SimpleChange.SimpleChangeListener;
import model_client.Balle;
import model_client.Jeu;
import model_client.Raquette;
import model_client.User;

public class VueStats extends JPanel implements SimpleChangeListener{
	
	public VueStats(){
		setLayout(new BorderLayout());
		
		/**/
		Jeu jeu = new Jeu();
		jeu.add(new User("Toto"));
		jeu.add(new User("Titi"));		
		/**/
		
		TableUsers table = new TableUsers(jeu);
		table.setBackground(Color.LIGHT_GRAY);
		add(table, BorderLayout.NORTH);
		JPanel count = new JPanel();
		count.setLayout(new GridLayout(2,1));
		count.add(new VCountEchange());
		count.add(new VCountBalle());
		add(count, BorderLayout.SOUTH);
	}
	
	public void stateChanged(Object source) {
		repaint();		
	}
}