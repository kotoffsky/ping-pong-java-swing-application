package vue_client;

import javax.swing.JLabel;

public class VCountEchange extends JLabel{
	public static int nb_echanges = 0;
	
	public VCountEchange()
	{
		super("     Nombre d'échanges : " + nb_echanges + "     ");
	}

	public int getNb_echanges() {
		return nb_echanges;
	}

	public void setNb_echanges(int nb_echanges) {
		this.nb_echanges = nb_echanges;
	}
}
