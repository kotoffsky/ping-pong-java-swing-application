package vue_client;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.JFrame;

import model_client.Jeu;
import model_client.Raquette;
import model_client.User;

public class PongGUI extends JFrame {
	private VueJeu jeu;
	private VueStats stats;
	private VueConnexion connexion;
	private Jeu jeuModel = new Jeu();
	
	public PongGUI() {
		this.jeu = new VueJeu(jeuModel);
		this.stats = new VueStats();
		this.connexion = new VueConnexion();
		
		Container container = getContentPane();
		container.setLayout(new BorderLayout());
		container.add(connexion, BorderLayout.NORTH);
		container.add(jeu, BorderLayout.CENTER);
		container.add(stats, BorderLayout.LINE_END);

		pack();
		setTitle("Super pong !");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(1300,740);
		this.setVisible(true);
		
		
				
	}
	
}
