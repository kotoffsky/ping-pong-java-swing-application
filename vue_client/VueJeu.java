package vue_client;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import model_client.Jeu;
import model_client.User;
import SimpleChange.SimpleChangeListener;

public class VueJeu extends JPanel implements SimpleChangeListener, MouseMotionListener{
	
	private ArrayList<Vue2D> vues;
	
	private Jeu jeuModel;
	//private List<User> listeUsers = new ArrayList<User>();
	
	public VueJeu(Jeu jeuModel)
	{
		this.jeuModel = jeuModel;
		
		/**/
		User user1 = new User("Emiliano");
		User user2 = new User("Nick");
		User user3 = new User("Helen");
		User user4 = new User("Laurence");
		/**/
		/**/
		this.jeuModel.getListUsers().add(user1);
		this.jeuModel.getListUsers().add(user2);
		this.jeuModel.getListUsers().add(user3);
		this.jeuModel.getListUsers().add(user4);
		/**/
		
		this.vues = new ArrayList<Vue2D>();
		this.setBackground(Color.WHITE);
		vues.add(new Balle2D(Color.PINK));
		//vues.add(new Raquette2D(Color.BLUE));
		this.setFocusable(true);
		this.addMouseMotionListener(this);
	}
	
	public void addVue2D(Vue2D vue)
	{
		vues.add(vue);
		repaint();
	}
	
	public void removeVue2D(Vue2D vue)
	{
		vues.remove(vue);
		repaint();
	}
	
	/*public void addUser(User u){
		listeUsers.add(u);
	}*/
	@Override
	public void paintComponent(Graphics g)
	{
//		Graphics2D g2d = (Graphics2D) g;
		// On choisit une couleur de fond pour le rectangle
		g.setColor(Color.white);
		// On le dessine de sorte qu'il occupe toute la surface
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
//		// On redéfinit une couleur pour le rond
//		g.setColor(Balle.couleur);
//		// On le dessine aux coordonnées souhaitées
//		g.fillOval(this.balleInst.getX(), 
//				this.balleInst.getY(), 
//				Balle.diametre, 
//				Balle.diametre);
//		for (Vue2D v:vues)
//		{
//			v.draw(g2d);
//		}
		
		//super.paintComponent(g);
		for(User user : this.jeuModel.getListUsers()){
			g.setColor(Color.BLACK);
			g.fillRect(user.getRaquette().getPosX(), 
					this.getHeight() - user.getRaquette().height,
					user.getRaquette().width,
					user.getRaquette().height);
		}
		
	}
	
	@Override
	public void stateChanged(Object source) {
		repaint();		
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		/*Raquette2D r = (Raquette2D) this.vues.get(1);
		if(arg0.getX() >= (getWidth() - (r.width/2))) {
			r.setPosX((getWidth() - (r.width/2)), this);
		}
		else if(arg0.getX() <= r.width) {
			r.setPosX((r.width/2), this);
		}
		else
			r.setPosX(arg0.getX(), this);*/
		for(User user : this.jeuModel.getListUsers()){
			if (arg0.getX() < (this.getWidth() - user.getRaquette().width)) {
				user.getRaquette().setPosX(arg0.getX());
			repaint();
			}
		}
			
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		for(User user : this.jeuModel.getListUsers()){
			if (e.getX() < (this.getWidth() - user.getRaquette().width)) {
				user.getRaquette().setPosX(e.getX());
			repaint();
			}
		}
	}
}
