package vue_client;

import javax.swing.JLabel;

public class VCountBalle extends JLabel {
	private int nb_balles;
	
	public VCountBalle()
	{
		super();
		this.nb_balles = 0;
		this.setText("     Nombre de balles : " + nb_balles + "     ");
	}

	public int getNb_balles() {
		return nb_balles;
	}

	public void setNb_balles(int nb_balles) {
		this.nb_balles = nb_balles;
	}
}
