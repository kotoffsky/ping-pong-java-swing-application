package vue_client;

import java.awt.Color;
import java.awt.Graphics2D;

import javax.swing.JComponent;

import SimpleChange.*;

public abstract class Vue2D extends JComponent implements SimpleChangeListener {
	protected int height;
	protected int width;
	protected Color color;
	
	public Vue2D(int height, int width, Color color)
	{
		this.height = height;
		this.width = width;
		this.color = color;
	}
	
	public abstract void draw(Graphics2D g2D);
}
