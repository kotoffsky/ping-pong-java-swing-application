package vue_client;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model_client.User;
import server.Client;

public class VueConnexion extends JPanel implements ActionListener{
	
	private User user;
	private JTextField jtf;
	private JButton btnConnexion;
	
	public VueConnexion() {
		
		this.setLayout(new FlowLayout());
		
		jtf = new JTextField();
		jtf.setPreferredSize(new Dimension(200, 26));
		btnConnexion = new JButton("Connexion");
		btnConnexion.addActionListener(this);
		this.add(jtf);
		this.add(btnConnexion);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("Button clicked. Text is "+jtf.getText());
		this.user = new User(jtf.getText());
		System.out.println(user);
		new Client(this.user);
		this.setVisible(false);
		
	}
}
