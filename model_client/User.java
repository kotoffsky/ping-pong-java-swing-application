package model_client;

import java.io.Serializable;

import SimpleChange.SimpleChangeModel;

public class User extends SimpleChangeModel implements Serializable {

	private int id;
	private double score;
	private String nom;
	private Raquette raquette;
	
	public User(String nom) {
		this.score = 0;
		this.nom = nom;
		this.raquette = new Raquette();
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public Raquette getRaquette() {
		return raquette;
	}

	public void setRaquette(Raquette raquette) {
		this.raquette = raquette;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
}
