package model_client;

import java.io.Serializable;

import SimpleChange.SimpleChangeModel;

public class Balle extends SimpleChangeModel implements EntiteMobile, Serializable{

	int posX;
	int posY;
	
	public Balle() {
		this.posX = 0;
		this.posY = 0;
	}
	
	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	@Override
	public  void move() {
		this.posX += 1;
		this.posY += 1;
	}
}
