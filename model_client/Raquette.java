package model_client;

import java.io.Serializable;

import SimpleChange.SimpleChangeModel;

public class Raquette extends SimpleChangeModel implements EntiteMobile, Serializable {

	int posX;
	int posY;
	public int height = 20;
	public int width = 100;
	
	public Raquette() {
		this.posX = 580;
		this.posY = 700;
	}
	
	public int getPosX() {
		return posX;
	}


	public void setPosX(int posX) {
		this.posX = posX;
	}

	@Override
	public void move() {
		
	}
}
