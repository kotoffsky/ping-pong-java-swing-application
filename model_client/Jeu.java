package model_client;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;

import SimpleChange.SimpleChangeListener;
import SimpleChange.SimpleChangeModel;

public class Jeu extends SimpleChangeModel implements SimpleChangeListener {

	private List<User> listUsers;
	private Balle balle;

	public Jeu() {
		this.listUsers = new ArrayList<User>();
		this.balle = new Balle();
	}

	public Balle getBalle() {
		return balle;
	}

	public void setBalle(Balle balle) {
		this.balle = balle;
	}
	
	public void add(User u) {
		this.listUsers.add(u);
		u.addSimpleChangeListener(this);
	}
	
	public void remove(User u) {
		this.listUsers.remove(u);
		u.removeSimpleChangeListener(this);
	}
	
	public User getUser(int index) {
		return this.listUsers.get(index);
	}

	@Override
	public void stateChanged(Object source) {
		fireSimpleChange();
	}

	public List<User> getListUsers() {
		return listUsers;
	}

	public void setListUsers(List<User> listUsers) {
		this.listUsers = listUsers;
		fireSimpleChange();
	}
	
	public int getUsersCount() {
		return listUsers.size();
	}
}
