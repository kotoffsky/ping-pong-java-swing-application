package contoller;

import model_client.Jeu;
import server.Client;
import vue_client.PongGUI;

public class PongController {
	
	private Jeu jeu;
	private Client client;
	private PongGUI pongGUI;
	
	public PongController(Client client, Jeu jeu, PongGUI pongGUI) {
		this.client = client;
		this.jeu = jeu;
		this.pongGUI = pongGUI;
		jeu.add(client.getUser());
	}
}
