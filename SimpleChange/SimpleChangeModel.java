package SimpleChange;

import java.util.ArrayList;

public abstract class SimpleChangeModel implements SimpleChangeModelInterface {
	protected ArrayList<SimpleChangeListener> listenerList = new ArrayList<SimpleChangeListener>();
	
	public void addSimpleChangeListener(SimpleChangeListener listener)
	{
		listenerList.add(listener);
	}
	
	public void removeSimpleChangeListener(SimpleChangeListener listener)
	{
		listenerList.remove(listener);
	}
	
	protected void fireSimpleChange()
	{
		for(SimpleChangeListener l : listenerList)
		{
			l.stateChanged(this);
		}
	}
}
