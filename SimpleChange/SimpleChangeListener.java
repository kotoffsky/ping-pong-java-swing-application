package SimpleChange;

public interface SimpleChangeListener {
	void stateChanged(Object source);
}