package SimpleChange;

public interface SimpleChangeModelInterface {
	void addSimpleChangeListener(SimpleChangeListener listener);
	void removeSimpleChangeListener(SimpleChangeListener listener);
}
